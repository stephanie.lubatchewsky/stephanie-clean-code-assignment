package com.avenuecode.springessentials.service;

import com.avenuecode.springessentials.domain.Student;
import com.avenuecode.springessentials.repository.StudentRepository;
import com.avenuecode.springessentials.requests.StudentPostRequestBody;
import com.avenuecode.springessentials.requests.StudentPutRequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Objects;

@Service
public class StudentService {

    private final StudentRepository studentRepository;

    @Autowired
    public StudentService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    public List<Student> getStudents() {
            return studentRepository.findAll();
    }

    public Student getStudentById(Long studentId) {
        return studentRepository.findById(studentId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Student not found!"));
    }

    public Student addNewStudent(StudentPostRequestBody studentRequest) {
        var student = Student.builder()
                .name(studentRequest.getName())
                .email(studentRequest.getEmail())
                .dob(studentRequest.getDob())
                .build();
        return studentRepository.save(student);
    }

    public void deleteStudent(Long studentId) {
        studentRepository.deleteById(studentId);
    }

    public void updateStudent(StudentPutRequestBody studentRequest) {
        var student = Student.builder()
                .id(studentRequest.getId())
                .name(studentRequest.getName())
                .email(studentRequest.getEmail())
                .build();

        var oldStudent = studentRepository.findById(student.getId())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Student not found!"));

        if(student.getName() != null)
            updateName(oldStudent, student);

        if(student.getEmail() != null)
            updateEmail(oldStudent, student);
    }

    public void updateName(Student old, Student req) {
        if (Objects.equals(old.getName(), req.getName()))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Please, inform a different name!");

        old.setName(req.getName());
        this.studentRepository.save(old);
    }

    public void updateEmail(Student old, Student req) {
        if(studentRepository.findStudentByEmail(req.getEmail()).isPresent())
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "This email is already in use!");

        old.setEmail(req.getEmail());
        this.studentRepository.save(old);
    }
}
