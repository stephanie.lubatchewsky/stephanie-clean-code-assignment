package com.avenuecode.springessentials.repository;

import com.avenuecode.springessentials.domain.EssentialsUser;
import com.avenuecode.springessentials.domain.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EssentialsUserRepository extends JpaRepository<EssentialsUser, Long> {

    EssentialsUser findByUsername(String username);
}
