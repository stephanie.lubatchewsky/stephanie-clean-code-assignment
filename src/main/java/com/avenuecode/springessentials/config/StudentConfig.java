package com.avenuecode.springessentials.config;

import com.avenuecode.springessentials.domain.Student;
import com.avenuecode.springessentials.repository.StudentRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.util.List;

import static java.time.Month.SEPTEMBER;

@Configuration
public class StudentConfig {

    @Bean
    CommandLineRunner commandLineRunner(StudentRepository repository) {
        return args -> {
            Student alexa = new Student(
                    "Alexa",
                    "alexa@mail.com",
                    LocalDate.of(1993, SEPTEMBER, 23)
            );

            Student siri = new Student(
                    "Siri",
                    "siri@mail.com",
                    LocalDate.of(1996, SEPTEMBER, 23)
            );

            repository.saveAll(
                    List.of(alexa, siri)
            );
        };
    }
}
